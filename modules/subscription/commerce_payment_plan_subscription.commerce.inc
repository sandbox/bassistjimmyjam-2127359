<?php
/**
 * @file
 * Commerce hook implementations for the Commerce Payment Plan Subscription
 * module.
 */

/**
 * Implements hook_commerce_payment_plan_type_info()
 */
function commerce_payment_plan_subscription_commerce_payment_plan_type_info() {
  return array(
    'subscription' => array(
      'name' => t('Subscription'),
    ),
  );
}

/**
 * Implements hook_commerce_product_type_info().
 */
function commerce_payment_plan_subscription_commerce_product_type_info() {
  return array(
    'subscription' => array(
      'type' => 'subscription',
      'name' => t('Subscription'),
      'description' => t('A recurring product that continues to generate invoices at a set interval and ends at a certain date or number of invoices.'),
      'revision' => TRUE,
    ),
  );
}

/**
 * Implements commerce_payment_plan_TYPE_paid_in_full().
 *
 * Creates the first subscription payment for a subscription payment plan.
 */
function commerce_payment_plan_subscription_commerce_payment_plan_subscription_paid_in_full($line_item, $plan, $schedule = NULL) {
  // If there are multiple orders attached to this payment schedule then this is
  // just a normal subscription payment and we do not need to create a new
  // order.
  if (commerce_payment_plan_subscription_get_existing_line_items($schedule) > 1) {
    return;
  }

  $plan_wrapper = entity_metadata_wrapper('commerce_payment_plan', $plan);
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Set the start date on the schedule.
  $today = new DateTime();
  $schedule->data['start'] = $today->format('Y-m-d');
  $schedule->data['payments'] = 0;

  // Determine the due date for the first payment.
  $due_date = commerce_payment_plan_subscription_next_payment_date($schedule);

  // Create a new invoice.
  if ($due_date) {
    $order = entity_metadata_wrapper('commerce_order', commerce_order_new($schedule->uid, 'invoice'));
    $order->commerce_order_invoice_due_date->set($due_date->getTimestamp());
    $order->save();

    // Clone the line item to use as a new line item for the invoice.
    $invoice_line_item = clone $line_item;
    $invoice_line_item->line_item_id = NULL;
    $invoice_line_item->is_new = TRUE;
    $invoice_line_item->order_id = $order->order_id->value();

    // If the line item has a UUID set then clear that as well.
    if (!empty($invoice_line_item->uuid)) {
      $invoice_line_item->uuid = NULL;
    }

    // Set the amount on the line item.
    $invoice_line_item->data['subscription_amount'] = $plan_wrapper->commerce_plan_sub_amount->value();
    commerce_payment_plan_subscription_set_line_item_unit_price($invoice_line_item);
    commerce_line_item_save($invoice_line_item);

    // Add the line item to the invoice.
    $order->commerce_line_items[] = $invoice_line_item;
    $order->save();

    // Update and save the payment schedule.
    ++$schedule->data['payments'];
    $schedule->data['last_line_item'] = $invoice_line_item->line_item_id;
    commerce_payment_plan_schedule_save($schedule);

    // Schedule a task to create the next subscription invoice.
    commerce_payment_plan_schedule_create_task($schedule->schedule_id, $due_date->getTimestamp());
  }
}

/**
 * Implements hook_commerce_product_calculate_sell_price_line_item().
 */
function commerce_payment_plan_subscription_commerce_product_calculate_sell_price_line_item_alter($line_item) {
  commerce_payment_plan_subscription_set_line_item_unit_price($line_item);
}

/**
 * Implements hook_commerce_cart_line_item_refresh().
 */
function commerce_payment_plan_subscription_commerce_cart_line_item_refresh($line_item, $order_wrapper) {
  // Update the unit price of the line item and recalculate the order total.
  commerce_payment_plan_subscription_set_line_item_unit_price($line_item);
  commerce_order_calculate_total($order_wrapper->value());
}

/**
 * Implements hook_commerce_payment_plan_TYPE_schedule_task().
 */
function commerce_payment_plan_subscription_commerce_payment_plan_subscription_schedule_task($schedule, $plan) {
 // If the schedule has been canceled, then we have nothing to do here.
  if ($schedule->status == 'canceled') {
    return;
  }

  // Load the last line item that was created for this schedule.
  $last_line_item = commerce_line_item_load($schedule->data['last_line_item']);

  // Determine when the next subscription payment would be due.
  $due_date = commerce_payment_plan_subscription_next_payment_date($schedule);

  // If there is no due date, then this schedule has been completed and we can
  // update it's status.
  if (!$due_date) {
    $schedule->status = 'completed';
    commerce_payment_plan_schedule_save($schedule);
    return;
  }

  // Create a new invoice.
  $order = entity_metadata_wrapper('commerce_order', commerce_order_new($schedule->uid, 'invoice'));
  $order->commerce_order_invoice_due_date->set($due_date->getTimestamp());
  $order->save();

  // Create the new invoice line item.
  $invoice_line_item = clone $last_line_item;
  $invoice_line_item->line_item_id = NULL;
  $invoice_line_item->is_new = TRUE;
  $invoice_line_item->order_id = $order->order_id->value();

  // If the line item has a UUID set then clear that as well.
  if (!empty($invoice_line_item->uuid)) {
    $invoice_line_item->uuid = NULL;
  }

  // Set the amount on the line item.
  commerce_payment_plan_subscription_set_line_item_unit_price($invoice_line_item);
  commerce_line_item_save($invoice_line_item);

  // Add the line item to the invoice.
  $order->commerce_line_items[] = $invoice_line_item;
  $order->save();

  // Schedule a task to create the next payment once this one is due.
  commerce_payment_plan_schedule_create_task($schedule->schedule_id, $due_date->getTimestamp());

  // Update and save the payment schedule.
  ++$schedule->data['payments'];
  $schedule->data['last_line_item'] = $invoice_line_item->line_item_id;
  commerce_payment_plan_schedule_save($schedule);
}
