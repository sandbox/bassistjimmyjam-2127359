<?php
/**
 * @file
 * Page callbacks for the Commerce Payment Plan UI module.
 */

/**
 * Page callback to create a new payment plan.
 */
function commerce_payment_plan_ui_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);

  // Bypass the admin/commerce/payment-plans/add listing if only one payment
  // plan type is available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }

  return theme('payment_plan_add_list', array('content' => $content));
}

/**
 * Page callback to create or edit a payment plan.
 */
function commerce_payment_plan_ui_plan_form_wrapper($plan) {
  // Include the forms file from the payment plan module.
  module_load_include('inc', 'commerce_payment_plan', 'includes/commerce_payment_plan.forms');

  return drupal_get_form('commerce_payment_plan_form', $plan);
}

/**
 * Page callback for an overview of payment plan types.
 */
function commerce_payment_plan_ui_types_overview() {
  drupal_add_css(drupal_get_path('module', 'commerce_product') . '/theme/commerce_product.admin.css');

  $header = array(
    t('Name'),
    t('Operations'),
  );

  $rows = array();

  // Loop through all defined payment plan types.
  foreach (commerce_payment_plan_types() as $type => $info) {
    // Build the operation links for the current payment plan type.
    $links = menu_contextual_links('commerce-payment-plan-type', 'admin/commerce/payment-plans/types', array(strtr($type, array('_' => '-'))));

    // Add the payment plan type's row to the table's rows array.
    $rows[] = array(
      theme('payment_plan_type_admin_overview', array('payment_plan_type' => $info)),
      theme('links', array('links' => $links, 'attributes' => array('class' => 'links inline operations'))),
    );
  }

  // If no payment plan types are defined then add a standard empty row.
  if (empty($rows)) {
    $rows[] = array(
      array(
        'data' => t('No payment plan types exist for your store.'),
        'colspan' => 2,
      )
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Page callback to manage a product type.
 */
function commerce_payment_plan_ui_type_form_wrapper($type) {
  if (is_array($type)) {
    $info = $type;
  }
  else {
    $payment_plan_types = commerce_payment_plan_types();
    $info = $payment_plan_types[$type];
  }

  // Include the forms file from the payment plan ui module.
  module_load_include('inc', 'commerce_payment_plan_ui', 'includes/commerce_payment_plan_ui.forms');

  return drupal_get_form('commerce_payment_plan_ui_type_form', $info);
}
