<?php
/**
 * @file
 * Theme functions for the Commerce Payment Plan UI module.
 */

/**
 * Displays the list of available payment plans types for plan creation.
 */
function theme_payment_plan_add_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="commerce-payment-plan-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }

    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('No payment plan types exist for your store.') . '</p>';
  }

  return $output;
}

/**
 * Builds an overview of a payment plan types for display to an administrator.
 */
function theme_payment_plan_type_admin_overview($variables) {
  $info = $variables['payment_plan_type'];

  $output = check_plain($info['name']);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $info['type'])) . '</small>';
  $output .= '<div class="description">' . filter_xss_admin($info['description']) . '</div>';

  return $output;
}

