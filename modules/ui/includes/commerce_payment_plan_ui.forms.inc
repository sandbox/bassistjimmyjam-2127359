<?php
function commerce_payment_plan_ui_type_form($form, &$form_state, $payment_plan_type) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_payment_plan_ui') . '/includes/commerce_payment_plan_ui.forms.inc';

  // Store the initial payment plan type in the form state.
  $info = $payment_plan_type;
  $form_state['payment_plan_type'] = $payment_plan_type;

  $modules = module_list();
  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('This product type was provided by the @module module and can not be modified.', array('@module' => $modules[$info['module']])) . '</p>',
  );

  $form['payment_plan_type'] = array(
    '#tree' => TRUE,
  );

  $form['payment_plan_type']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $info['name'],
    '#description' => t('The human-readable name of this payment plan type. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#disabled' => TRUE,
    '#size' => 32,
  );

  $form['payment_plan_type']['type'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#default_value' => $info['type'],
    '#maxlength' => 32,
    '#disabled' => TRUE,
    '#description' => t('The machine-readable name of this payment_plan type. This name must contain only lowercase letters, numbers, and underscores, it must be unique.'),
  );

  $form['payment_plan_type']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Describe this payment plan type. The text will be displayed on the <em>Add new payment plan</em> page.'),
    '#default_value' => $info['description'],
    '#rows' => 3,
    '#disabled' => TRUE,
  );

  $form['payment_plan_type']['help'] = array(
    '#type' => 'textarea',
    '#title' => t('Explanation or submission guidelines'),
    '#description' => t('This text will be displayed at the top of the page when creating or editing payment plans of this type.'),
    '#default_value' => $info['help'],
    '#rows' => 3,
    '#disabled' => TRUE,
  );

  return $form;
}
