<?php
/**
 * @file
* Installation and update hooks for the Commerce Payment Plan Installment
* module.
*/

/**
 * Implements hook_enable().
*/
function commerce_payment_plan_installment_enable() {
  $defaults = array(
    'field' => array(
      'cardinality' => 1,
      'entity_types' => array('commerce_payment_plan'),
      'translatable' => FALSE,
      'locked' => TRUE,
    ),
    'instance' => array(
      'entity_type' => 'commerce_payment_plan',
      'bundle' => 'installment',
      'required' => TRUE,
    ),
  );

  $fields = array(
    'commerce_plan_initial' => array(
      'field' => array(
        'type' => 'commerce_price',
      ),
      'instance' => array(
        'label' => t('Initial payment'),
        'widget' => array(
          'type' => 'commerce_price_full',
          'weight' => 10,
        ),
      ),
    ),
    'commerce_plan_installments' => array(
      'field' => array(
        'type' => 'installments',
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      ),
      'instance' => array(
        'label' => t('Installments'),
        'widet' => array(
          'type' => 'installments_field_widget_default',
          'weight' => 20,
        ),
      ),
    ),
  );

  // Clear the field info cache before we get started.
  cache_clear_all('field_info_fields', 'cache_field');

  foreach ($fields as $field_name => $info) {
    $field = field_info_field($field_name);
    if (!$field) {
      $info['field']['field_name'] = $field_name;
      $info['field'] += $defaults['field'];
      $field = field_create_field($info['field']);
    }

    $instance = field_info_instance('commerce_payment_plan', $field_name, 'installment');
    if (!$instance) {
      $info['instance']['field_name'] = $field_name;
      $info['instance'] += $defaults['instance'];
      field_create_instance($info['instance']);
    }
  }

  // If the payment plans field has not been attached to the installment
  // product type then add a new instance.
  if (!field_info_instance('commerce_product', 'commerce_payment_plans', 'installment')) {
    $field = modulefield_info('commerce_payment_plans');
    $instance = $field['instance'];
    $instance['field_name'] = 'commerce_payment_plans';
    $instance['entity_type'] = 'commerce_product';
    $instance['bundle'] = 'installment';
    $instance['widget'] = array(
      'type' => 'options_select',
    );

    modulefield_create_instance('commerce_payment_plans', 'commerce_product', 'installment');
    modulefield_field_create_instance($instance);
  }
}
