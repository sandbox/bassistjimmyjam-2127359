<?php
/**
 * @file
 * Commerce hook implementations for the Commerce Payment Plan Installment
 * module.
 */

/**
 * Implements hook_commerce_payment_plan_type_info()
 */
function commerce_payment_plan_installment_commerce_payment_plan_type_info() {
  return array(
    'installment' => array(
      'name' => t('Installment'),
    ),
  );
}

/**
 * Implements hook_commerce_product_type_info().
 */
function commerce_payment_plan_installment_commerce_product_type_info() {
  return array(
    'installment' => array(
      'type' => 'installment',
      'name' => t('Installment'),
      'description' => t('A product that can be paid for with a set payment plan rather than one single payment.'),
      'revision' => TRUE,
    ),
  );
}

/**
 * Implements hook_commerce_payment_plan_TYPE_cart_add().
 *
 * Adds any past due installments to the initial order.
 */
function commerce_payment_plan_installment_commerce_payment_plan_installment_cart_add($line_item, $plan, $schedule) {
  if (!empty($line_item->data['installment'])) {
    return;
  }

  // Iterate over the installment schedule on the payment plan, looking for any
  // installments that are past due.
  $past_due = $upcoming = array();
  $today = strtotime('today');
  $plan_wrapper = entity_metadata_wrapper('commerce_payment_plan', $plan);
  foreach ($plan_wrapper->commerce_plan_installments as $installment) {
    if ($installment->date->value() <= $today) {
      $past_due[$installment->date->value()] = $installment->value();
    }
    else {
      $upcoming[$installment->date->value()] = $installment->value();
    }
  }

  // Set the initial payment on the line item.
  $line_item->data['initial'] = $plan_wrapper->commerce_plan_initial->value();
  commerce_line_item_save($line_item);

  // Set the installments on the schedule.
  $schedule->data['installments'] = array(
    'past' => $past_due,
    'upcoming' => $upcoming,
  );
  commerce_payment_plan_schedule_save($schedule);

  // Iterate over the past due installments, adding a new line item for each.
  $order_updated = FALSE;;
  commerce_order_load_multiple();
  $order = entity_metadata_wrapper('commerce_order', $line_item->order_id);
  foreach ($past_due as $installment) {
    // Clone the initial line item to create a line item for the past due
    // installment.
    $installment_line_item = clone $line_item;
    $installment_line_item->line_item_id = NULL;
    $installment_line_item->is_new = TRUE;
    unset($installment_line_item->data['initial']);
    $installment_line_item->data['installment'] = $installment;

    // If the line item has a UUID set then clear that as well.
    if (!empty($installment_line_item->uuid)) {
      $installment_line_item->uuid = NULL;
    }

    // Add the line item to the order.
    commerce_line_item_save($installment_line_item);
    $order->commerce_line_items[] = $installment_line_item;
    $order_updated = TRUE;
  }

  // Only save the order if we updated it.
  if ($order_updated) {
    $order->save();
  }
}

/**
 * Implements commerce_payment_plan_TYPE_paid_in_full().
 *
 * Creates invoices for all remaining installment payments.
 */
function commerce_payment_plan_installment_commerce_payment_plan_installment_paid_in_full($line_item, $plan, $schedule) {
  // If this is the initial payment but there are not upcoming installments then
  // this schedule is complete.
  if (!empty($line_item->data['initial']) && empty($schedule->data['installments']['upcoming'])) {
    $schedule->status = 'completed';
    commerce_payment_plan_schedule_save($schedule);

    return;
  }
  elseif (empty($line_item->data['initial'])) {
    // This is not the initial, but we should see if this is there are any
    // outstanding installment payments. If not, then we can mark the schedule
    // complete.
    if (!commerce_payment_plan_schedule_has_outstanding_orders($schedule->schedule_id, $line_item->order_id)) {
      $schedule->status = 'completed';
      commerce_payment_plan_schedule_save($schedule);
    }

    return;
  }

  // If there are no remaining installments or this is not the initial payment
  // then we have nothing to create.
  if (empty($schedule->data['installments']['upcoming']) || empty($line_item->data['initial'])) {
    return;
  }

  // Clone the line item to use as the base for new line items.
  $base_line_item = clone $line_item;
  $base_line_item->line_item_id = NULL;
  $base_line_item->is_new = TRUE;
  unset($base_line_item->data['initial']);

  // If the line item has a UUID set then clear that as well.
  if (!empty($base_line_item->uuid)) {
    $base_line_item->uuid = NULL;
  }

  // Iterate over all upcoming installments, creating a new invoice for each
  // one.
  foreach ($schedule->data['installments']['upcoming'] as $due => $installment) {
    // Create the new order.
    $order = entity_metadata_wrapper('commerce_order', commerce_order_new($schedule->uid, 'invoice'));
    $order->commerce_order_invoice_due_date->set($due);
    $order->save();

    // Create the new line item
    $installment_line_item = clone $base_line_item;
    $installment_line_item->data['installment'] = $installment;
    $installment_line_item->order_id = $order->order_id->value();
    commerce_line_item_save($installment_line_item);

    // Add the line item to the order and save it.
    $order->commerce_line_items[] = $installment_line_item;
    commerce_cart_order_refresh($order);
    $order->save();

    // Move the installment to the scheduled index on the schedule.
    unset($schedule->data['installments']['upcoming'][$due]);
    $schedule->data['installments']['scheduled'][$due] = $installment;
  }

  // Save the payment schedule.
  commerce_payment_plan_schedule_save($schedule);
}

/**
 * Implements hook_commerce_product_calculate_sell_price_line_item().
 *
 * Sets the selling price of a line item for an installment payment plan.
 */
function commerce_payment_plan_installment_commerce_product_calculate_sell_price_line_item_alter($line_item) {
  commerce_payment_plan_installments_update_line_item_unit_price($line_item);
}

/**
 * Implements hook_commerce_cart_line_item_refresh().
 */
function commerce_payment_plan_installment_commerce_cart_line_item_refresh($line_item, $order_wrapper) {
  // Update the unit price of the line item and recalculate the order total.
  commerce_payment_plan_installments_update_line_item_unit_price($line_item);
  commerce_order_calculate_total($order_wrapper->value());
}

/**
 * Implements hook_commerce_payment_plan_line_item_title_alter().
 *
 * Adds a suffix to installment line item titles to identify whether it is an
 * initial or installment payment.
 */
function commerce_payment_plan_installment_commerce_payment_plan_line_item_title_alter(&$title, $line_item) {
  // Make sure the payment plan is an installment.
  $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  if ($wrapper->commerce_payment_plan_reference->type->value() != 'installment') {
    return;
  }

  // If there are no installment payments for this schedule then we should not
  // alter the title.
  $schedule = $wrapper->commerce_payment_schedule_ref->value();
  if (
    empty($schedule->data['installments']['past'])
    && empty($schedule->data['installments']['upcoming'])
    && empty($schedule->data['installments']['scheduled'])
  ) {
    return;
  }

  // Determine whether this line item is an initial or installment payment and
  // update the title appropriately.
  if (!empty($line_item->data['initial'])) {
    $title = t('!title Initial Payment', array('!title' => $title));
  }
  elseif (!empty($line_item->data['installment'])) {
    $title = t('!title Installment Payment', array('!title' => $title));
  }
}
