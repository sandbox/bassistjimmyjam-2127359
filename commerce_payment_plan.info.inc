<?php
/**
 * @file
 * Provides metadata for entity types provided by the Commerce Payment Plan
 * module.
 */

/**
 * Implements hook_entity_property_info().
 */
function commerce_payment_plan_entity_property_info() {
  $info = array();

  // Add meta-data about the basic commerce_payment_plan properties.
  $properties = &$info['commerce_payment_plan']['properties'];

  $properties['plan_id'] = array(
    'label' => t('Plan ID'),
    'description' => t('The internal numeric ID of the payment plan.'),
    'type' => 'integer',
    'schema field' => 'plan_id',
  );
  $properties['type'] = array(
    'label' => t('Type'),
    'description' => t('The type of the payment plan.'),
    'type' => 'token',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_payment_plan entities',
    'options list' => 'commerce_payment_plan_type_options_list',
    'required' => TRUE,
    'schema field' => 'type',
  );
  $properties['title'] = array(
    'label' => t('Title'),
    'description' => t('The title of the payment plan.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'title',
  );
  $properties['status'] = array(
    'label' => t('Status'),
    'description' => t('Boolean indicating whether the payment plan is active or disabled.'),
    'type' => 'boolean',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_payment_plan entities',
    'schema field' => 'status',
  );
  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the payment plan was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_payment_plan entities',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t('Date updated'),
    'description' => t('The date the payment plan was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'query callback' => 'entity_metadata_table_query',
    'setter permission' => 'administer commerce_payment_plan entities',
    'schema field' => 'changed',
  );
  $properties['uid'] = array(
    'label' => t('Creator ID'),
    'type' => 'integer',
    'description' => t('The unique ID of the payment plan creator.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_payment_plan entities',
    'clear' => array('creator'),
    'schema field' => 'uid',
  );
  $properties['creator'] = array(
    'label' => t('Creator'),
    'type' => 'user',
    'description' => t('The creator of the payment plan.'),
    'getter callback' => 'commerce_payment_plan_get_properties',
    'setter callback' => 'commerce_payment_plan_get_properties',
    'setter permission' => 'administer commerce_payment_plan entities',
    'required' => TRUE,
    'computed' => TRUE,
    'clear' => array('uid'),
  );

  // Add meta-data about the basic commerce_payment_schedule properties.
  $properties = &$info['commerce_payment_schedule']['properties'];

  $properties['schedule_id'] = array(
    'label' => t('Schedule ID'),
    'description' => t('The internal numeric ID of the payment schedule.'),
    'type' => 'integer',
    'schema field' => 'schedule_id',
  );
  $properties['type'] = array(
    'label' => t('Type'),
    'description' => t('The type of the payment plan referenced by the payment schedule.'),
    'type' => 'token',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'type',
  );
  $properties['title'] = array(
    'label' => t('Title'),
    'description' => t('The title of the payment schedule.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'title',
  );
  $properties['status'] = array(
    'label' => t('Status'),
    'description' => t('Boolean indicating whether the payment schedule is active or disabled.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'commerce_payment_plan_schedule_status_get_title',
    'setter permission' => 'administer commerce_payment_schedule entities',
    'schema field' => 'status',
  );
  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the payment schedule was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_payment_schedule entities',
    'schema field' => 'created',
  );
  $properties['uid'] = array(
    'label' => t('Owner'),
    'type' => 'user',
    'description' => t('The unique ID of the user that the payment schedule is assigned to.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_payment_schedule entities',
    'clear' => array('owner'),
    'schema field' => 'uid',
  );
  $properties['line_item_id'] = array(
    'label' => t('Initial line item'),
    'type' => 'commerce_line_item',
    'description' => t('The unique ID of the line item that represents the original purchase for the payment schedule.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer commerce_payment_schedule entities',
    'schema field' => 'line_item_id',
  );

  return $info;
}

/**
 * Wraps commerce_payment_plan_type_get_name() for the Entity module.
 */
function commerce_payment_plan_type_options_list() {
  return commerce_payment_plan_type_get_name();
}

/**
 * Callback for getting payment plan properties.
 *
 * @see commerce_payment_plan_entity_property_info()
 */
function commerce_payment_plan_get_properties($plan, array $options, $name) {
  switch ($name) {
    case 'creator':
      return $plan->uid;

    case 'edit_url':
      return url('admin/commerce/payment-plans/' . $plan->plan_id . '/edit', $options);
  }
}
