<?php
/**
 * @file
 * Views hook implementations for the Commerce Payment Plan module.
 */

/**
 * Implements hook_views_data().
 *
 * @todo Add commerce_payment_subscription data.
 */
function commerce_payment_plan_views_data() {
  $data = array(
    'commerce_payment_plan' => _commerce_payment_plan_views_data_payment_plan(),
    'commerce_payment_plan_revision' => _commerce_payment_plan_views_data_payment_plan_revision(),
    'commerce_payment_schedule' => _commerce_payment_plan_views_data_payment_schedule(),
  );

  // If the date_views module is enabled then find all date fields and replace
  // their filter handler with the appropriate handler from the date_views
  // module.
  if (module_exists('date_views')) {
    foreach ($data as $table => &$fields) {
      foreach ($fields as $field_name => &$definition) {
        if (isset($definition['filter']['handler']) && 'views_handler_filter_date' == $definition['filter']['handler']) {
          $definition['filter']['handler'] = 'date_views_filter_handler_simple';
        }
      }
    }
  }

  return $data;
}

/**
 * Provides the views definition for payment plan entities.
 *
 * @return array
 *   Views definition for payment plans.
 */
function _commerce_payment_plan_views_data_payment_plan() {
  $data['table']['group']  = t('Commerce Payment Plan');

  $data['table']['base'] = array(
    'field' => 'plan_id',
    'title' => t('Commerce Payment Plan'),
    'help' => t('Payment plans for commerce purchases.'),
    'access query tag' => 'commerce_payment_plan_access',
  );
  $data['table']['entity type'] = 'commerce_payment_plan';

  // Expose the plan ID.
  $data['plan_id'] = array(
    'title' => t('Plan ID'),
    'help' => t('The unique internal identifier of the payment plan.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the plan type.
  $data['type'] = array(
    'title' => t('Type'),
    'help' => t('The human-readable name of the type of the payment plan.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the plan title.
  $data['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of the payment plan.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the plan status.
  $data['status'] = array(
    'title' => t('Status'),
    'help' => t('Whether or not the payment plan is active.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'active-disabled' => array(t('Active'), t('Disabled')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the created and changed timestamps.
  $data['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the payment plan was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['created_fulldate'] = array(
    'title' => t('Created date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['created_year_month'] = array(
    'title' => t('Created year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['created_timestamp_year'] = array(
    'title' => t('Created year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['created_month'] = array(
    'title' => t('Created month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['created_day'] = array(
    'title' => t('Created day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['created_week'] = array(
    'title' => t('Created week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );

  $data['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the payment plan was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['changed_fulldate'] = array(
    'title' => t('Updated date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['changed_year_month'] = array(
    'title' => t('Updated year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['changed_timestamp_year'] = array(
    'title' => t('Updated year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['changed_month'] = array(
    'title' => t('Updated month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['changed_day'] = array(
    'title' => t('Updated day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['changed_week'] = array(
    'title' => t('Updated week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );

  return $data;
}

/**
 * Provides the views definition for payment plan revisions.
 *
 * @return array
 *   Views definition for payment plans.
 */
function _commerce_payment_plan_views_data_payment_plan_revision() {
  // Get the views data for payment plan entities and update the table values.
  $data = _commerce_payment_plan_views_data_payment_plan();
  $data['table']['group'] = t('Commerce Payment Plan (historical data)');
  $data['table']['base']['field'] = 'revision_id';
  $data['table']['base']['title'] = t('Commerce Payment Plan (historical data)');
  $data['table']['base']['help'] = t('Revisions for payment plans.');

  // Unset the type and all created/changed fields.
  unset($data['type']);
  foreach ($data as $key => $info) {
    if (preg_match('/^created|changed/', $key)) {
      unset($data[$key]);
    }
  }

  // Expose the revision ID.
  $data['revision_id'] = array(
    'title' => t('Revision ID'),
    'help' => t('The unique internal identifier of the revision.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Add a relationship to the plan.
  $data['plan_id']['relationship'] = array(
    'base' => 'commerce_payment_plan',
    'base field' => 'plan_id',
    'relationship field' => 'plan_id',
    'label' => t('Payment plan'),
  );

  return $data;
}

/**
 * Provides the views definition for payment plan schedule.
 *
 * @return array
 *   Views definition for payment schedules.
 */
function _commerce_payment_plan_views_data_payment_schedule() {
  $data['table']['group']  = t('Commerce Payment Schedule');

  $data['table']['base'] = array(
    'field' => 'schedule_id',
    'title' => t('Commerce Payment Schedule'),
    'help' => t('Payment schedules for commerce payment plan purchases.'),
    'access query tag' => 'commerce_payment_schedule_access',
  );
  $data['table']['entity type'] = 'commerce_payment_schedule';

  $data['owner'] = array(
    'title' => t('Owner'),
    'help' => t('User that the payment schedule has been assigned to.'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'relationship field' => 'uid',
      'label' => t('Owner'),
    ),
  );

  $data['commerce_payment_plan_revision'] = array(
    'title' => t('Payment plan revision'),
    'help' => t('The payment plan that is referenced by the payment schedule at the revision when the schedule was created.'),
    'relationship' => array(
      'base' => 'commerce_payment_plan_revision',
      'base field' => 'revision_id',
      'relationship field' => 'plan_revision_id',
      'label' => t('Payment plan revision'),
    ),
  );

  $data['commerce_product_revision'] = array(
    'title' => t('Product revision'),
    'help' => t('The product that is referenced by the payment schedule at the revision when the schedule was created.'),
    'relationship' => array(
      'base' => 'commerce_product_revision',
      'base field' => 'revision_id',
      'relationship field' => 'product_revision_id',
      'label' => t('Product revision'),
    ),
  );

  $data['initial_line_item'] = array(
    'title' => t('Initial line item'),
    'help' => t('The initial line item that the payment schedule was created for.'),
    'relationship' => array(
      'base' => 'commerce_line_item',
      'base field' => 'line_item_id',
      'relationship field' => 'line_item_id',
      'label' => t('Initial line item'),
    ),
  );

  // Expose the schedule ID.
  $data['schedule_id'] = array(
    'title' => t('Schedule ID'),
    'help' => t('The unique internal identifier of the payment schedule.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the plan type.
  $data['type'] = array(
    'title' => t('Type'),
    'help' => t('The human-readable name of the type of the payment plan referenced by the subscription.'),
    'field' => array(
      'handler' => 'commerce_payment_plan_handler_field_schedule_type',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'commerce_payment_plan_handler_filter_schedule_type',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the schedule title.
  $data['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of the payment schedule.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the schedule status.
  $data['status'] = array(
    'title' => t('Status'),
    'help' => t('The workflow status of the payment schedule.'),
    'field' => array(
      'handler' => 'commerce_payment_plan_handler_field_schedule_status',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'commerce_payment_plan_handler_filter_schedule_status',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Expose the created and next payment timestamps.
  $data['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the payment schedule was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['created_fulldate'] = array(
    'title' => t('Created date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  $data['created_year_month'] = array(
    'title' => t('Created year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  $data['created_timestamp_year'] = array(
    'title' => t('Created year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  $data['created_month'] = array(
    'title' => t('Created month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  $data['created_day'] = array(
    'title' => t('Created day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  $data['created_week'] = array(
    'title' => t('Created week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );

  return $data;
}
