<?php
/**
 * @file
 * Default views for the Commer Payment Plan module.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_payment_plan_views_default_views() {
  $views = array();

  // Default view used by the commerce_payment_plan_reference field to select a
  // payment plan when adding a product to the user's cart.
  $view = new view();
  $view->name = 'commerce_payment_plan_line_item_payment_plans';
  $view->description = 'Lists payment plans available for a line item based on the product.';
  $view->tag = 'commerce, payment plans';
  $view->base_table = 'commerce_payment_plan';
  $view->human_name = 'Line item payment plans';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_commerce_payment_plans_commerce_product']['id'] = 'reverse_commerce_payment_plans_commerce_product';
  $handler->display->display_options['relationships']['reverse_commerce_payment_plans_commerce_product']['table'] = 'commerce_payment_plan';
  $handler->display->display_options['relationships']['reverse_commerce_payment_plans_commerce_product']['field'] = 'reverse_commerce_payment_plans_commerce_product';
  $handler->display->display_options['relationships']['reverse_commerce_payment_plans_commerce_product']['label'] = 'Product';
  $handler->display->display_options['relationships']['reverse_commerce_payment_plans_commerce_product']['required'] = TRUE;
  /* Field: Commerce Payment Plan: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_payment_plan';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Sort criterion: Commerce Payment Plan: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'commerce_payment_plan';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: Commerce Product: Product ID */
  $handler->display->display_options['arguments']['product_id']['id'] = 'product_id';
  $handler->display->display_options['arguments']['product_id']['table'] = 'commerce_product';
  $handler->display->display_options['arguments']['product_id']['field'] = 'product_id';
  $handler->display->display_options['arguments']['product_id']['relationship'] = 'reverse_commerce_payment_plans_commerce_product';
  $handler->display->display_options['arguments']['product_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['product_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['product_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['product_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['product_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Commerce Payment Plan: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'commerce_payment_plan';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;

  $views[$view->name] = $view;

  return $views;
}
