<?php
/**
 * @file
 * Contains commerce_payment_plan_handler_filter_schedule_status.
 */

/**
 * Filter by payment schedule status.
 */
class commerce_payment_plan_handler_filter_schedule_status extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Status');
      $this->value_options = array();
      foreach (commerce_payment_plan_schedule_status_get_title() as $status => $title) {
        $this->value_options[$status] = $title;
      }
    }
  }
}
