<?php
/**
 * @file
 * Contains commerce_payment_plan_handler_filter_schedule_type.
 */

/**
 * Filter by payment schedule type.
 */
class commerce_payment_plan_handler_filter_schedule_type extends views_handler_filter_in_operator {
  function get_value_options() {
  if (!isset($this->value_options)) {
      $this->value_title = t('Type');
      $this->value_options = commerce_payment_plan_type_get_name();
    }
  }
}
