<?php
/**
 * @file
 * Contains commerce_payment_plan_handler_field_schedule_status.
 */

/**
 * Field handler to translate a payment schedule status into its readable form.
 */
class commerce_payment_plan_handler_field_schedule_status extends views_handler_field {
  function render($values) {
    $status = $this->get_value($values);
    $statuses = commerce_payment_plan_schedule_status_get_title($status);

    return $statuses[$status];
  }
}
