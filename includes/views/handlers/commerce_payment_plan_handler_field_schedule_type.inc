<?php
/**
 * @file
* Contains commerce_payment_plan_handler_field_schedule_type.
*/

/**
 * Field handler to translate a payment schedule type into its readable form.
*/
class commerce_payment_plan_handler_field_schedule_type extends views_handler_field {
  function render($values) {
    $type = $this->get_value($values);
    if ($type) {
      $names = commerce_payment_plan_type_get_name($type);

      return (isset($names[$type]) ? $names[$type] : '');
    }
  }
}
