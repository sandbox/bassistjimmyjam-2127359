<?php
/**
 * @file
 * Form callbacks for the Commerce Payment Plan module.
 */

/**
 * Form callback: create or edit a payment plan.
 *
 * @param stdClass $plan
 *   The plan object to edit or for a create form an empty plan object with only
 *   the type defined.
 */
function commerce_payment_plan_form($form, &$form_state, $plan) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_payment_plan') . '/includes/commerce_group_fee.forms.inc';

  // Add the default field elements.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => (!empty($plan->title) ? $plan->title : NULL),
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  // Add the field related form elements.
  $form_state['commerce_payment_plan'] = $plan;
  $langcode = LANGUAGE_NONE;
  field_attach_form('commerce_payment_plan', $plan, $form, $form_state, $langcode);

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#description' => t("Disabled payment plans will not be selectable when adding a product to a user's cart checkout."),
    '#options' => array(
      '1' => t('Active'),
      '0' => t('Disabled'),
    ),
    '#default_value' => $plan->status,
    '#required' => TRUE,
  );

  // Add the submit button.
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save payment plan'),
    '#submit' => array('commerce_payment_plan_form_submit'),
    '#validate' => array('commerce_payment_plan_form_validate'),
  );

  return $form;
}

/**
 * Validation callback for the payment plan add/update form.
 */
function commerce_payment_plan_form_validate($form, &$form_state) {
  // Notify field widgets to validate their data.
  field_attach_form_validate('commerce_payment_plan', $form_state['commerce_payment_plan'], $form, $form_state);
}

function commerce_payment_plan_form_submit($form, &$form_state) {
  global $user;

  // Save default parameters back into the payment plan object.
  $plan = &$form_state['commerce_payment_plan'];
  $plan->title = $form_state['values']['title'];
  $plan->status = $form_state['values']['status'];

  // Set the payment plan's uid if it's being created at this time.
  if (empty($plan->plan_id)) {
    $plan->uid = $user->uid;
  }

  // Always create a new revision.
  $plan->revision = TRUE;

  // Notify field widgets.
  field_attach_submit('commerce_payment_plan', $plan, $form, $form_state);

  // Save the payment plan.
  commerce_payment_plan_save($plan);

  // Inform the user that the payment plan was saved.
  drupal_set_message(t('Payment plan saved.'));
}
