<?php
/**
 * @file
 * Contains CommercePaymentPlanEntityController and
 * CommercePaymentScheduleEntityController.
 */

/**
 * Entity controller for commerce payment plan entities.
 */
class CommercePaymentPlanEntityController extends DrupalCommerceEntityController {
  /**
   * Create a default payment plan.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *
   * @return stdCalss
   *   A payment plan object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $values += array(
      'plan_id' => NULL,
      'is_new' => TRUE,
      'title' => '',
      'status' => 1,
      'created' => '',
      'changed' => '',
      'revision_id' => NULL,
    );

    return parent::create($values);
  }

  /**
   * Saves a payment plan.
   *
   * @param stdClass $plan
   *   The full payment plan object to save.
   * @param DatabaseTransaction $transaction
   *   An optional transaction object.
   *
   * @return integer
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($plan, DatabaseTransaction $transaction = NULL) {
    global $user;

    // Hardcode the changed time.
    $plan->changed = REQUEST_TIME;

    if (empty($plan->{$this->idKey}) || !empty($plan->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($plan->created)) {
        $plan->created = REQUEST_TIME;
      }
    }

    // Determine if we will be inserting a new payment plan.
    $plan->is_new = empty($plan->plan_id);

    // Always create a new revision.
    $plan->revision = TRUE;
    if (!isset($plan->log)) {
      $plan->log = '';
    }

    return parent::save($plan, $transaction);
  }

  /**
   * Unserializes the data property of loaded payment plans.
   */
  public function attachLoad(&$queried_entities, $revision_id = FALSE) {
    foreach ($queried_entities as $entity_id => &$entity) {
      $entity->data = unserialize($entity->data);
    }

    // Call the default attachLoad() method. This will add fields and invoke
    // hooks.
    parent::attachLoad($queried_entities, $revision_id);
  }
}

/**
 * Entity controller for commerce payment schedule entities.
 */
class CommercePaymentScheduleEntityController extends DrupalCommerceEntityController {
  /**
   * Create a default payment schedule.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *
   * @return stdCalss
   *   A payment schedule object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $values += array(
      'schedule_id' => NULL,
      'is_new' => TRUE,
      'title' => '',
      'status' => 'active',
      'created' => '',
      'changed' => '',
    );

    return parent::create($values);
  }

  /**
   * Saves a payment schedule.
   *
   * @param stdClass $schedule
   *   The full payment schedule object to save.
   * @param DatabaseTransaction $transaction
   *   An optional transaction object.
   *
   * @return integer
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($schedule, DatabaseTransaction $transaction = NULL) {
    global $user;

    if (empty($schedule->{$this->idKey}) || !empty($schedule->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($schedule->created)) {
        $schedule->created = REQUEST_TIME;
      }
    }

    // Determine if we will be inserting a new schedule.
    $schedule->is_new = empty($schedule->schedule_id);

    return parent::save($schedule, $transaction);
  }

  /**
   * Unserializes the data property of loaded payment schedules.
   */
  public function attachLoad(&$queried_schedules, $revision_id = FALSE) {
    foreach ($queried_schedules as &$schedule) {
      $schedule->data = unserialize($schedule->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_commerce_payment_plan_schedule_load().
    parent::attachLoad($queried_schedules, $revision_id);
  }
}
