<?php
/**
 * @file
 * Documentation of hooks provided by the Commerce Payment Plan module.
 */

/**
 * Allows modules to define their own payment plan types.
 *
 * @return array
 *   Definition of one or more payment plan types, keyed on the machine-readable
 *   type, with the following indexes:
 *   - type: The machine-readable type of the payment plan. Should be the same
 *     as the key for the payment method type.
 *   - name: The human-readable name of the payment plan type.
 *   - description: A description of the payment plan type.
 *   - revision: Whether or not to always create new revisions for payment plans
 *     of this type.
 *
 * @see commerce_payment_plan_subscription_commerce_product_type_info()
 */
function hook_commerce_payment_plan_type_info() {
  return array(
    'example' => array(
      'type' => 'example',
      'name' => t('Example'),
      'description' => t('Example payment plan type.'),
      'revision' => TRUE,
    ),
  );
}

/**
 * Allows other modules to alter the payment method type definitions.
 *
 * @param array $types
 *   Array of all payment plan types.
 *
 * @see hook_commerce_payment_plan_type_info()
 */
function hook_commerce_payment_plan_type_info_alter(&$types) {
  $types['example']['revision'] = FALSE;
}

/**
 * Allow module to define their own payment schedule statuses.
 *
 * @return array
 *   Array of payment schedule statuses defined by the module, keyed on the
 *   machine-readable status name, with the following indexes:
 *   - title: The human-readable name of the status.
 *   - weight: The weight of the status as it should appear in select lists.
 *   - active: Whether or not the status indiciates a currently active payment
 *     schedule.
 *
 * @see commerce_payment_plan_commerce_payment_plan_schedule_status_info().
 */
function hook_commerce_payment_plan_schedule_status_info() {
  return array(
    'deferred' => array(
      'title' => t('Deferred'),
      'weight' => -50,
      'active' => FALSE,
    ),
  );
}

/**
 * Allows other modules to alter the available payment schedule statuses.
 *
 * @param array $statuses
 *   Array of defined payment schedule statuses.
 *
 * @see hook_commerce_payment_plan_schedule_status_info()
 */
function hook_commerce_payment_plan_schedule_status_info_alter(&$statuses) {
  $statuses['canceled']['weight'] = 10;
}

/**
 * Notifies other modules that a payment plan has been added to a user's cart.
 *
 * @param stdClass $line_item
 *   The line item that was added to the user's cart.
 * @param stdClass $plan
 *   The payment plan for the line item. If the line item had an existing
 *   payment schedule set on it, this plan would be the revision referenced in
 *   that schedule. It is recommended to consider this entity as read-only.
 * @param stdClass $schedule
 *   The payment schedule for the line item, if appropriate. If the line item
 *   has the payment plan reference field but not the payment schedule reference
 *   field then no schedule will be provided.
 */
function hook_commerce_payment_plan_cart_add($line_item, $plan, $schedule = NULL) {
  if (!empty($schedule->is_new)) {
    drupal_set_message(t('A new payment schedule has been created for your purchase.'));
  }
}

/**
 * Notifies other modules that a payment plan has been added to a user's cart.
 *
 * This hook allows modules to be notified only when a payment plan of a
 * specific type has been added to the user's cart.
 *
 * @param stdClass $line_item
 *   The line item that was added to the user's cart.
 * @param stdClass $plan
 *   The payment plan for the line item. If the line item had an existing
 *   payment schedule set on it, this plan would be the revision referenced in
 *   that schedule. It is recommended to consider this entity as read-only.
 * @param stdClass $schedule
 *   The payment schedule for the line item, if appropriate. If the line item
 *   has the payment plan reference field but not the payment schedule reference
 *   field then no schedule will be provided.
 *
 * @see hook_commerce_payment_plan_cart_add()
 */
function hook_commerce_payment_plan_TYPE_cart_add($line_item, $plan, $schedule = NULL) {
  // See previous example.
}

/**
 * Notifies other modules that a payment plan line item has been paid for.
 *
 * @param stdClass $line_item
 *   The line item from the order that was paid for that includes the payment
 *   plan.
 * @param stdClass $plan
 *   The payment plan from the line item. If the line item had a payment
 *   schedule set on it, this plan would be the revision referenced in that
 *   schedule. It is recommended to consider this entity as read-only.
 * @param stdClass $schedule
 *   The payment schedule for the line item, if appropriate. If the line item
 *   has the payment plan reference field but not the payment schedule reference
 *   field then no schedule will be provided.
 */
function hook_commerce_payment_plan_paid_in_full($line_item, $plan, $schedule = NULL) {
  drupal_set_message(t('A payment has been made towards your payment plan.'));
}

/**
 * Notifies other modules that a payment plan line item has been paid for.
 *
 * @param stdClass $line_item
 *   The line item from the order that was paid for that includes the payment
 *   plan.
 * @param stdClass $plan
 *   The payment plan from the line item. If the line item had a payment
 *   schedule set on it, this plan would be the revision referenced in that
 *   schedule. It is recommended to consider this entity as read-only.
 * @param stdClass $schedule
 *   The payment schedule for the line item, if appropriate. If the line item
 *   has the payment plan reference field but not the payment schedule reference
 *   field then no schedule will be provided.
 *
 * @see hook_commerce_payment_plan_paid_in_full()
 */
function hook_commerce_payment_plan_TYPE_paid_in_full($line_item, $plan, $schedule = NULL) {
  // See previous example.
}

/**
 * Allow other modules to react when processing a payment schedule task.
 *
 * This hook is called from a queue process. The queue process itself does not
 * perform any processing; therefore, at least one module must implement this
 * hook or the more targeted hook below for a task to be processed.
 *
 * @param stdClass $schedule
 *   The payment schedule that the task is being processed for.
 * @param stdClass $plan
 *   The payment plan from the payment schedule at the referenced revision. It
 *   is recommended to consider this entity as read-only.
 */
function hook_commerce_payment_plan_schedule_task($schedule, $plan) {
  // Create a new order for the payment schedule.
  // ...

  // Create a new task for this schedule.
  $date = new DateTime();
  $date->add(new DateInterval('P30D'));
  commerce_payment_plan_schedule_create_task($schedule_id, $date->getTimestamp());
}

/**
 * Allow other modules to react when processing a payment schedule task.
 *
 * This hook is called from a queue process. The queue process itself does not
 * perform any processing; therefore, at least one module must implement this
 * hook or the more generic hook above for a task to be processed.
 *
 * @param stdClass $schedule
 *   The payment schedule that the task is being processed for.
 * @param stdClass $plan
 *   The payment plan from the payment schedule at the referenced revision. It
 *   is recommended to consider this entity as read-only.
 *
 * @see hook_commerce_payment_plan_schedule_task()
 */
function hook_commerce_payment_plan_TYPE_schedule_task($schedule, $plan) {
  // See previous example.
}

/**
 * Allows other modules to alter the title for a payment plan line item.
 *
 * @param string $title
 *   The current title of the line item.
 * @param stdClass $line_item
 *   The line item the title is for.
 */
function hook_commerce_payment_plan_line_item_title_alter(&$title, $line_item) {
  $title .= t(' Custom Suffix');
}
