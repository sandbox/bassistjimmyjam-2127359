<?php
/**
 * @file
 * Commerce hook implementations for the Commerce Payment Plan module.
 */

/**
 * Implements hook_commerce_payment_plan_schedule_status_info().
 */
function commerce_payment_plan_commerce_payment_plan_schedule_status_info() {
  return array(
    'canceled' => array(
      'title' => t('Canceled'),
      'active' => FALSE,
      'weight' => -100,
    ),
    'active' => array(
      'title' => t('Active'),
      'active' => TRUE,
      'weight' => 0,
    ),
    'completed' => array(
      'title' => t('Completed'),
      'active' => FALSE,
      'weight' => 100,
    ),
  );
}

/**
 * Implements hook_commerce_line_item_type_info().
 */
function commerce_payment_plan_commerce_line_item_type_info() {
  return array(
    'payment_plan' => array(
      'type' => 'payment_plan',
      'name' => t('Payment plan'),
      'description' => t('Line item type for use with products that utilize payment plans.'),
      'product' => TRUE,
      'add_form_submit_value' => t('Add product'),
      'base' => 'commerce_product_line_item',
      'callbacks' => array(
        'title' => 'commerce_payment_plan_line_item_title',
      ),
    ),
  );
}

/**
 * Implements hook_commerce_cart_product_add().
 *
 * When a payment plan is added to a user's cart, creates a new payment schedule
 * (if applicable) and notifies other modules.
 */
function commerce_payment_plan_commerce_cart_product_add($order, $product, $quantity, $line_item) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // If the line item type does not utilize the payment plan reference field or
  // it is empty then bail.
  if (
    empty($line_item_wrapper->commerce_payment_plan_reference)
    || !$line_item_wrapper->commerce_payment_plan_reference->value()
  ) {
    return;
  }

  // If the line item utilizes the payment schedule reference field and it is
  // not set then we need to create a new payment schedule.
  $schedule = NULL;
  if (!empty($line_item_wrapper->commerce_payment_schedule_ref)) {
    $schedule = $line_item_wrapper->commerce_payment_schedule_ref->value();
    if (empty($schedule)) {
      $plan = $line_item_wrapper->commerce_payment_plan_reference->value();
      $schedule = commerce_payment_plan_schedule_new($plan, $product);
      $schedule->uid = $order->uid;
      $schedule->line_item_id = $line_item->line_item_id;

      // TODO: Build a title for the payment schedule.
      commerce_payment_plan_schedule_save($schedule);

      $line_item_wrapper->commerce_payment_schedule_ref->set($schedule);
    }
    else {
      $plan = commerce_payment_plan_load_revision($schedule->plan_id, $schedule->plan_revision_id);
    }
  }

  // TODO: Create rules events for these hooks.
  // Let other users know about that a new payment plan line item was added.
  module_invoke_all('commerce_payment_plan_cart_add', $line_item, $plan, $schedule);
  module_invoke_all('commerce_payment_plan_' . $plan->type . '_cart_add', $line_item, $plan, $schedule);

  // Save the line item.
  $line_item_wrapper->save();
}

/**
 * Implements hook_commerce_payment_order_paid_in_full().
 */
function commerce_payment_plan_commerce_payment_order_paid_in_full($order, $transaction) {
  // Iterate over all of the line items in the order, looking for any with
  // payment plans.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $line_item) {
    // If the line item type does not utilize the payment plan reference field or
    // it is empty then move on to the next.
    if (
      empty($line_item->commerce_payment_plan_reference)
      || !$line_item->commerce_payment_plan_reference->value()
    ) {
      continue;
    }

    // If the line item utilizes the payment schedule reference field then load
    // the schedule and the correct plan revision. Otherwise, load the current
    // revision of the payment plan.
    if (!empty($line_item->commerce_payment_schedule_ref)) {
      $schedule = $line_item->commerce_payment_schedule_ref->value();
      $plan = current(entity_load(
        'commerce_payment_plan',
        array($schedule->plan_id),
        array('revision_id' => $schedule->plan_revision_id)
      ));
    }
    else {
      $schedule = NULL;
      $plan = $line_item->commerce_payment_plan_reference->value();
    }

    // TODO: Create rules events for these hooks.
    // Let other users know about that a payment plan line item was paid for.
    module_invoke_all('commerce_payment_plan_paid_in_full', $line_item->value(), $plan, $schedule);
    module_invoke_all('commerce_payment_plan_' . $plan->type . '_paid_in_full', $line_item->value(), $plan, $schedule);
  }
}
