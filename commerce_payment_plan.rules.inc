<?php
/**
 * @file
 * Rules hook implementations for the Commerce Payment Plan module.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_payment_plan_rules_action_info() {
  $actions = array();

  $actions['commerce_payment_plan_schedule_update_status'] = array(
    'label' => t('Update the schedule status'),
    'parameter' => array(
      'commerce_payment_plan_schedule' => array(
        'type' => 'commerce_payment_schedule',
        'label' => t('Schedule to update'),
      ),
      'schedule_status' => array(
        'type' => 'text',
        'label' => t('Schedule status'),
        'options list' => 'commerce_payment_plan_schedule_status_get_title',
      ),
    ),
    'group' => t('Commerce Payment Schedule'),
    'callbacks' => array(
      'execute' => 'commerce_payment_plan_rules_update_schedule_status',
    ),
  );

  return $actions;
}

/**
 * Rules action: updates a payment schedule's status.
 */
function commerce_payment_plan_rules_update_schedule_status($schedule, $status) {
  commerce_payment_plan_schedule_statue_update($schedule, $status);
}
